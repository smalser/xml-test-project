import os
import multiprocessing
from zipfile import ZipFile
from xml.etree import ElementTree


def process_et(f):
    et = ElementTree.parse(f)
    root = et.getroot()
    id = root[0].get('value')
    value = root[1].get('value')

    levels = {'id': id, 'level': value}
    objects = [{'id': id, 'object_name': obj.get('name')} for obj in root[2]]
    return levels, objects


def func(filename, dir='zips'):
    zf = ZipFile(f'{dir}/{filename}', 'r')
    lines = []
    for x in zf.filelist:
        with zf.open(x.filename) as f:
            lines.append(process_et(f))

    return lines


def process_results(results):
    levels, objects = [], []
    for x, y in results:
        levels.append(x)
        objects.extend(y)

    with open('levels.csv', 'w', encoding='utf-8') as f:
        f.write('id, level\n')
        f.write('\n'.join([f'{x["id"]}, {x["level"]}' for x in levels]))

    with open('objects.csv', 'w', encoding='utf-8') as f:
        f.write('id, object_name\n')
        f.write(''.join([f'{x["id"]}, {x["object_name"]}\n' for x in objects]))


def main():
    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    pool_map = pool.map_async(func, os.listdir('zips'))
    results = sum(pool_map.get(), [])
    process_results(results)


if __name__ == '__main__':
    main()
