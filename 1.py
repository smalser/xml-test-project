import os
import uuid
from random import randint
from xml.etree import ElementTree
from zipfile import ZipFile
from io import BytesIO


def append_var(root: ElementTree.Element, name: str, value: str):
    el = ElementTree.Element('var')
    el.set('name', name)
    el.set('value', value)
    root.append(el)


def add_objects(root: ElementTree.Element):
    el = ElementTree.Element('objects')
    for _ in range(randint(1, 10)):
        se = ElementTree.SubElement(el, 'object')
        se.set('name', str(uuid.uuid4()))
    root.append(el)


def create_xml():
    root = ElementTree.Element('root')
    append_var(root, 'id', str(uuid.uuid4()))
    append_var(root, 'level', str(randint(0, 100)))
    add_objects(root)

    bio = BytesIO()
    tree = ElementTree.ElementTree(root)
    tree.write(bio)
    bio.seek(0)
    return bio


def create_zip(number: int):
    zf = ZipFile(open(f'zips/{number}.zip', 'wb'), 'w')
    for i in range(100):
        bs = create_xml().read()
        zf.writestr(f'{i}.xml', bs)
    zf.close()


def main():
    try:
        os.mkdir('zips')
    except FileExistsError:
        pass

    for i in range(50):
        create_zip(i)


if __name__ == '__main__':
    main()
